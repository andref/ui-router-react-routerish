module.exports = (api) => {
  const isTest = api.env('test');
  let envOpts = {};

  if (isTest) {
    envOpts = {
      targets: {
        node: 'current'
      }
    };
  }

  return {
    presets: [
      ['@babel/preset-env', envOpts],
      '@babel/preset-react',
      '@babel/preset-typescript'
    ]
  };
};
