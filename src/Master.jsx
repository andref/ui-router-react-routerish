import * as React from 'react';
import * as angular from 'angular';
import { react2angular } from 'react2angular';
import { Route, Link, NavLink } from './router.tsx';

function Empty() {
  return <div className="split-right list">Listing</div>;
}

function Edit({ params }) {
  return (
    <div className="split-right edit">
      Editing {params.id}.{' '}
      <Link className="button" role="button" to="master.view" params={params}>
        Cancel
      </Link>
    </div>
  );
}

function View({ params }) {
  return (
    <div className="split-right view">
      Viewing {params.id}.{' '}
      <Link className="button" role="button" to="master.edit" params={params}>
        Edit
      </Link>
    </div>
  );
}

function List({ items, selection, onSelectionChanged }) {
  return (
    <div className="split-left list">
      {items.map((item) => (
        <NavLink
          key={item}
          className="list-item"
          to="master.view"
          params={{ id: item }}
          activeClassName="active"
          onClick={() => onSelectionChanged(item)}
        >
          Item #{item}
        </NavLink>
      ))}
    </div>
  );
}

export function Master(props) {
  const [selection, setSelection] = React.useState(null);
  const [items, setItems] = React.useState(
    [...Array(5).keys()].map((it) => it + 1)
  );

  return (
    <div className="app">
      <div className="split">
        <Route state="master.*">
          <List
            items={items}
            selection={selection}
            onSelectionChanged={setSelection}
          />
        </Route>
        <Route state="master.list" component={Empty} />
        <Route state="master.view" component={View} />
        <Route state="master.edit" component={Edit} />
      </div>
    </div>
  );
}

Master.AngularJS = react2angular(Master, [], []);
