import * as angular from 'angular';
import router from '@uirouter/angularjs';
import { Master } from './Master';

angular
  .module('app', [router])
  .component('master', Master.AngularJS)
  .config(routes);

function routes($stateProvider, $urlRouterProvider, $transitionsProvider) {
  $stateProvider
    .state({
      name: 'master',
      url: '/master',
      redirectTo: 'master.list',
      component: 'master'
    })
    .state({
      name: 'master.list',
      url: '',
      template: ''
    })
    .state({
      name: 'master.view',
      url: '/view/{id}',
      template: ''
    })
    .state({
      name: 'master.edit',
      url: '/edit/{id}',
      template: ''
    });

  $urlRouterProvider.when('/', '/master');
  $urlRouterProvider.when('', '/master');
}
