import * as React from 'react';
import {
  matchState,
  StateObject,
  StateService,
  Transition,
  TransitionService
} from '@uirouter/angularjs';
import { useInjector } from './hooks';

/** Parâmetros consumidos por um estado da aplicação. */
type StateParams = { [key: string]: any };

interface ILinkProps extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
  /** Estado para onde aplicação deve navegar quando o Link for clicado */
  to: string;

  /** Parâmetros que devem ser passados ao estado. */
  params?: StateParams;
}

/**
 * Permite navegar pela aplicação especificando estados e não URLs. O elemento <a> gerado pelo
 * <Link> vai conter a URL correta.
 */
export function Link({ to, params, onClick, children, ...props }: ILinkProps) {
  const $state = useInjector<StateService>('$state');
  const href = $state.href(to, params || {});
  const handleClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
    if (onClick) {
      onClick(e);
    }
    if (!e.defaultPrevented) {
      e.preventDefault();
      $state.go(to, params);
    }
  };
  return (
    <a href={href} onClick={handleClick} {...props}>
      {children}
    </a>
  );
}

interface INavLinkProps extends ILinkProps {
  /** Classe CSS que o NavLink deve receber se estiver ativo. */
  activeClassName?: string;

  /** Estilos CSS que o NavLink deve receber se estiver ativo. */
  activeStyle?: React.CSSProperties;

  /**
   * Qual o valor do atributo `aria-current` o NavLink deve receber se estiver ativo. O padrão é
   * "page".
   */
  ariaCurrent:
    | boolean
    | 'false'
    | 'true'
    | 'page'
    | 'step'
    | 'location'
    | 'date'
    | 'time'
    | undefined;
}

/**
 * Versão especial do componente <Link> que adiciona atributos de estilo quando o destino
 * corresponde ao estado atual.
 */
export function NavLink({
  to,
  params,
  activeClassName,
  activeStyle,
  ariaCurrent,
  className,
  style,
  ...props
}: INavLinkProps) {
  const $state = useInjector<StateService>('$state');
  const $transitions = useInjector<TransitionService>('$transitions');
  const [active, setActive] = React.useState($state.includes(to, params));

  React.useEffect(() => {
    const unregisterOnSuccess = $transitions.onSuccess(
      {},
      (transition: Transition) => {
        setActive($state.includes(to, params));
      }
    );
    return () => {
      unregisterOnSuccess();
    };
  }, []);

  ariaCurrent = ariaCurrent || 'page';
  ariaCurrent = (active && ariaCurrent) || undefined;
  if (active) {
    className = [className, activeClassName].filter((it) => !!it).join(' ');
    style = { ...style, ...activeStyle };
  }

  return (
    <Link
      to={to}
      params={params}
      aria-current={ariaCurrent}
      className={className}
      style={style}
      {...props}
    />
  );
}

interface Match {
  params: StateParams;
}

interface IRouteProps {
  /**
   * Quando a aplicação navegar para este estado, o conteúdo do <Route> será mostrado. Este atributo
   * pode ser o nome de um estado (ex. person.edit) ou um glob (ex. person.*).
   */
  state: string;

  /**
   * Componente que será renderizado quando a aplicação navegar para este <Route>. O componente
   * receberá como propriedades uma instância de `Match`.
   */
  component?: React.ReactType;

  /**
   * Conteúdo a ser mostrado quando a aplicação navegar para este <Route>. Será ignorado se a prop
   * `component` tiver sido especificada. Pode ser uma função e, nesse caso, receberá como
   * argumento uma instância de `Match`.
   */
  children?: React.ReactNode | ((match: Match) => React.ReactNode);

  /**
   * Render prop que produz o conteúdo a ser mostrado quando a aplicação navegar para este
   * <Route>. Será ignorado se `component` ou se `children` tiverem sido especificados. Recebe
   * como argumento uma instância de `Match`.
   */
  render?: (match: Match) => React.ReactNode;
}

/**
 * Renderiza uma parte da interface quando a aplicação navega para um estado que corresponde ao seu
 * estado (ou glob).
 */
export function Route({ state, component, render, children }: IRouteProps) {
  const $state = useInjector<StateService>('$state');
  const $transitions = useInjector<TransitionService>('$transitions');
  const [current, setCurrent] = React.useState(
    matchState($state.$current, state, $state.transition)
  );
  const [params, setParams] = React.useState<StateParams>($state.params);

  React.useEffect(() => {
    const unregisterOnEnter = $transitions.onEnter(
      { to: state },
      (transition) => {
        setCurrent(true);
        setParams(transition.params());
        return true;
      }
    );
    const unregisterOnExit = $transitions.onExit(
      { from: state },
      (transition) => {
        // Otimização: se o `state` for um glob ele pode englobar tanto o estado de origem como o
        // estado de destino. Se setássemos false aqui para depois setar true no onEnter, o conteúdo
        // sofreria um unmount e um mount desnecessários. Em vez disso, somente alteramos o estado
        // `current` pra false se o estado de destino não for englobado pelo `state`.

        const targetState = transition.$to();
        if (!matchState(targetState, state, transition)) {
          setCurrent(false);
        }
        return true;
      }
    );
    return () => {
      unregisterOnEnter();
      unregisterOnExit();
    };
  }, [state]);

  if (!current) {
    return null;
  }

  // Render the children of this Route using, in order of preference, component, render or children.
  // A Match object containing the current state params is passed to whichever strategy is used.

  const match = { params };
  if (component) {
    return React.createElement(component, match);
  } else if (render) {
    return render(match) || null;
  } else if (children) {
    if (typeof children === 'function') {
      const l = children(match);
      children = children(match);
    }
    return children || null;
  } else {
    return null;
  }
}
