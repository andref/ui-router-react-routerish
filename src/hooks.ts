import * as angular from 'angular';

export function useInjector<T>(name: string) {
  const injector = angular.element(document.body).injector();
  return injector.get(name) as T;
}
