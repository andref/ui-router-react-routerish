import { shallow, mount } from 'enzyme';
import * as React from 'react';
import { Link, NavLink } from './router';

jest.mock('./hooks');

import { useInjector } from './hooks';

const $state = {
  href: () => 'hello/world',
  go: jest.fn().mockName('$state.go'),
  includes: jest.fn().mockName('$state.includes')
};

const $transitions = {
  onSuccess: jest.fn().mockName('$transitions.onSuccess')
};

useInjector.mockImplementation((name) => {
  if (name === '$state') return $state;
  if (name === '$transitions') return $transitions;
  return null;
});

beforeEach(() => {
  [$state.go, $state.includes, $transitions.onSuccess].forEach((mock) =>
    mock.mockClear()
  );
});

describe('Interop Router', () => {
  describe('<Link>', () => {
    it('renderiza o conteúdo como conteúdo da tag <a>', () => {
      const wrapper = shallow(<Link to="test">Hello</Link>);
      expect(wrapper.find('a').text()).toEqual('Hello');
    });

    it('usa a URL retornada pelo $state como href da tag <a>', () => {
      const wrapper = shallow(<Link to="test">Hello</Link>);
      expect(wrapper.find('a').prop('href')).toEqual('hello/world');
    });

    it('muda o estado quando clicado', () => {
      const event = {
        preventDefault: jest.fn().mockName('e.preventDefault()'),
        defaultPrevented: false
      };
      const wrapper = shallow(<Link to="test">Hello</Link>);

      wrapper.find('a').simulate('click', event);

      expect($state.go).toBeCalledWith('test', undefined);
    });

    it('informa os parâmetros quando muda de estado', () => {
      const event = {
        preventDefault: jest.fn().mockName('e.preventDefault()'),
        defaultPrevented: false
      };
      const wrapper = shallow(
        <Link to="test" params={{ id: 10 }}>
          Hello
        </Link>
      );

      wrapper.find('a').simulate('click', event);

      expect($state.go).toBeCalledWith('test', { id: 10 });
    });

    it('chama o onClick externo quando clicado', () => {
      const event = {
        preventDefault: jest.fn().mockName('e.preventDefault()'),
        defaultPrevented: false
      };
      const onClick = jest.fn().mockName('onClick');
      const wrapper = shallow(
        <Link to="test" onClick={onClick}>
          Hello
        </Link>
      );

      wrapper.find('a').simulate('click', event);

      expect(onClick).toBeCalledWith(event);
    });

    it('não muda o estado se event.defaultPrevented for true', () => {
      const event = {
        preventDefault: jest.fn().mockName('e.preventDefault()'),
        defaultPrevented: true
      };

      const wrapper = shallow(<Link to="test">Hello</Link>);

      wrapper.find('a').simulate('click', event);

      expect($state.go).not.toBeCalled();
    });
  });

  describe('<NavLink>', () => {
    it('Não adiciona activeClassName se não estiver ativo', () => {
      const wrapper = mount(
        <NavLink to="test" activeClassName="active">
          Hello
        </NavLink>
      );
      expect(wrapper.find('a').prop('className')).toBeUndefined();
    });

    it('Não adiciona activeStyle se não estiver ativo', () => {
      const wrapper = mount(
        <NavLink to="test" activeStyle={{ color: 'red' }}>
          Hello
        </NavLink>
      );
      expect(wrapper.find('a').prop('className')).toBeUndefined();
    });
  });
});
